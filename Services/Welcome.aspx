﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Welcome.aspx.cs" Inherits="Authorization.Services.Welcome" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <link rel="stylesheet" href="../Content/bootstrap.min.css"/>
    <link rel="stylesheet" href="Style.css"/>
    <title>Welcome</title>
</head>
<body>
    <form id="form1" runat="server">
        <nav class="navbar navbar-expand-lg navbar-light bg-dark">
           <div class="container">
                <a class="navbar-brand text-white-50" href="#">Navbar</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mx-auto">
                    <li class="nav-item active mr-3">
                        <a class="nav-link text-white-50" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item mr-3">
                        <a class="nav-link text-white-50" href="#">Link</a>
                    </li>
                    <li class="nav-item dropdown mr-3">
                        <a class="nav-link dropdown-toggle text-white-50" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </li>
                    <li class="nav-item mr-3">
                        <a class="nav-link disabled text-white-50" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
                    </li>
                </ul>
                <asp:Button type="button" ID="logout" runat="server" Text="Logout" class="btn btn-primary px-4" OnClick="logout_Click"/>
                <%--<button type="button" class="btn btn-primary px-4">Logout</button>--%>
            </div>
           </div>
        </nav>
        <section class="welcome-section padding">
            <div class="container">
                <div class="row justify-content-center align-items-center">
                    <h2>
                        <asp:HyperLink ID="welcome" runat="server" Visible="false">HyperLink</asp:HyperLink>
                    </h2>
                </div>
            </div>
        </section>
    </form>
    <script src="../Scripts/jquery-3.0.0.min.js"></script>
    <script src="../Scripts/bootstrap.min.js"></script>
    <script src="../Scripts/popper.min.js"></script>
</body>
</html>
