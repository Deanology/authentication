﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Registration.aspx.cs" Inherits="Authorization.Services.Registration" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="../Content/bootstrap.min.css"/>
    <link rel="stylesheet" href="Style.css"/>
    <title>Registration</title>
</head>
<body>
    <%--<form id="form1" runat="server">
        <div>
           
        </div>
    </form>--%>
   <section>
       <div class="container">
           <div class="row justify-content-center align-items-center">
               <div class="col-md-7">
                   <div class="registration_form_container bg-dark mt-lg-5">
                   <form method="post" id="registration_form" class="registration_form p-5" runat="server">
                       <h2 class="form-title text-center text-uppercase text-white-50">Create Account</h2>                    
                       <div class="form-group">
                           <asp:TextBox ID="fullname" class="form-control fullname" runat="server" Text="Enter your fullname"></asp:TextBox>
                           <%-- <input type="text" class="form-control firstname" id="firstname" aria-describedby="emailHelp" placeholder="Enter your fullname" />--%>
                       </div>
                       <div class="form-group">
                           <asp:TextBox ID="email" class="form-control email" runat="server" Text="Enter your email address"></asp:TextBox>
                            <%--<input type="email" class="form-control email" id="email" aria-describedby="email" placeholder="Enter your email address" />--%>
                       </div>
                       <div class="form-group">
                           <asp:TextBox ID="password" class="form-control password" runat="server" Text="Enter your password"></asp:TextBox>
                            <%--<input type="password" class="form-control" id="password" aria-describedby="password" placeholder="Password" />--%>
                       </div>
                       <div class="form-group">
                           <asp:TextBox ID="confirm_password" class="form-control confirm_password" runat="server" Text="Confirm Password"></asp:TextBox>
                            <%--<input type="password" class="form-control" id="confirm_password" aria-describedby="confirm_password" placeholder="Confirm Password" />--%>
                       </div>
                       <div class="form-group">
                           <asp:Button ID="submit" class="form-control" runat="server" Text="Sign up" OnClick="submit_Click" />
                            <%--<input type="submit" class="form-control" id="submit" aria-describedby="submit" value="Sign up" />--%>
                       </div>
                       <p class="mt-3 text-center text-white-50 text-decoration-none">Have already an account ? <a href="Login.aspx" class="font-weight-bold">Login here</a></p>
                   </form>
               </div>
               </div>
               
           </div>
       </div>
   </section>
    <script src="../Scripts/jquery-3.0.0.min.js"></script>
    <script src="../Scripts/bootstrap.min.js"></script>
    <script src="../Scripts/popper.min.js"></script>
    
</body>
</html>
