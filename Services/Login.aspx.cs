﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace Authorization.Services
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User"] != null)
            {
                Response.Redirect("Welcome.aspx");
            }
        }

        protected void submit_Click(object sender, EventArgs e)
        {
            SignIn();
        }
        void SignIn()
        {
            //create a connection string
            string connection = ConfigurationManager.ConnectionStrings["dbconnection"].ConnectionString;

            try
            {
                //use the sql connection class
                SqlConnection connect = new SqlConnection(connection);


                //check if connection is open
                if (connect.State == ConnectionState.Closed)
                    connect.Open();

                //always parametirized queries
                SqlCommand cmd = new SqlCommand("SELECT * FROM Users WHERE Email =@email AND Password =@password", connect);
                cmd.Parameters.AddWithValue("@email", email.Text.Trim());
                cmd.Parameters.AddWithValue("@password", password.Text.Trim());

                //create a sql data adapter
                SqlDataAdapter data = new SqlDataAdapter(cmd);

                //create a datatable
                DataTable dataTable = new DataTable();

                //fill data table with data
                data.Fill(dataTable);

                if (dataTable.Rows.Count >= 1)
                {
                    Session["User"] = email.Text.Trim();
                    Response.Redirect("Welcome.aspx");
                }
                    
                else
                    Response.Write("<script>alert('Incorrect Login Details');</script>");
               
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }

        }
    }
}