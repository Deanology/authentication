﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace Authorization.Services
{
    public partial class Registration : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void submit_Click(object sender, EventArgs e)
        {
            if (CheckMemberExistence())
            {
                Response.Write("<script>alert('User Already Exist with this Email Address, try another email address');</script>");
            }
            else
            {
                string connection = ConfigurationManager.ConnectionStrings["dbconnection"].ConnectionString;

                try
                {
                    //use the sql connection class
                    SqlConnection connect = new SqlConnection(connection);


                    //check if connection is open
                    if (connect.State == ConnectionState.Closed)
                        connect.Open();

                    //use the insert query command
                    SqlCommand cmd = new SqlCommand("INSERT INTO Users (Email, Password, ConfirmPassword, Fullname)" +
                    "VALUES(@email, @password, @confirm_password, @fullname)", connect);

                    //assigning values to the placeholders 
                    cmd.Parameters.AddWithValue("@email", email.Text.Trim());
                    cmd.Parameters.AddWithValue("@password", password.Text.Trim());
                    cmd.Parameters.AddWithValue("@confirm_password", confirm_password.Text.Trim());
                    cmd.Parameters.AddWithValue("@fullname", fullname.Text.Trim());

                    //execute query and insert form values into the database
                    cmd.ExecuteNonQuery();
                    //close connection
                    connect.Close();
                    //Go to the Login Page and Login
                    Response.Redirect("Login.aspx");
                }
                catch (Exception ex)
                {
                    Response.Write(ex.Message);
                }
            }
            //create a connection string
            
        }

        bool CheckMemberExistence()
        {
            string connection = ConfigurationManager.ConnectionStrings["dbconnection"].ConnectionString;

            try
            {
                //use the sql connection class
                SqlConnection connect = new SqlConnection(connection);


                //check if connection is open
                if (connect.State == ConnectionState.Closed)
                    connect.Open();


                //use the insert query command
                SqlCommand cmd = new SqlCommand("SELECT * FROM Users WHERE Email = " + email.Text.Trim(), connect);

                //create a sql data adapter
                SqlDataAdapter data = new SqlDataAdapter(cmd);

                //create a datatable
                DataTable dataTable = new DataTable();

                //fill data table with data
                data.Fill(dataTable);

                if (dataTable.Rows.Count >= 1)
                    return true;
                else
                    return false;


                connect.Close();
                //Go to the Login Page and Login
                /*Response.Redirect("Login.aspx");*/
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
                return false;
            }
           
        }
       
    }
}