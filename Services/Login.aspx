﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Authorization.Services.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="../Content/bootstrap.min.css"/>
    <link rel="stylesheet" href="Style.css"/>
    <title>Login</title>
</head>
<body>
   <%-- <form id="form1" runat="server">
        <div>
        </div>
    </form>--%>
    <section>
        <div class="container">
            <div class="row justify-content-center align-items-center">
                <div class="col-md-7">
                    <div class="login_form_container bg-dark mt-5">
                        <form method="post" id="registration_form" class="registration_form p-5" runat="server">
                            <h2 class="form-title text-center text-uppercase text-white-50">Login</h2>
                            <div class="form-group">
                                <asp:TextBox ID="email" class="form-control email" runat="server" Text="Enter your email address" ></asp:TextBox>
                                <%--<input type="email" class="form-control email" id="email" aria-describedby="email" placeholder="Enter your email address" />--%>
                            </div>
                            <div class="form-group">
                                <asp:TextBox ID="password" class="form-control password" runat="server" Text="Enter your password" ></asp:TextBox>
                                <%--<input type="password" class="form-control password" id="password" aria-describedby="password" placeholder="Enter your password" />--%>
                            </div>
                            <div class="form-group">
                                <asp:Button ID="submit" class="form-control" runat="server" Text="Login" OnClick="submit_Click" />
                                <%--<input type="submit" class="form-control" id="submit" aria-describedby="submit" value="Login" />--%>
                            </div>
                            
                            <p class="mt-3 text-center text-white text-decoration-none">Don't have an account ? <a href="Registration.aspx" class="font-weight-bold">Register</a></p>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <script src="../Scripts/jquery-3.0.0.min.js"></script>
    <script src="../Scripts/bootstrap.min.js"></script>
    <script src="../Scripts/popper.min.js"></script>
</body>
</html>
